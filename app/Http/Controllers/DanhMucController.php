<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Caster\RedisCaster;

class DanhMucController extends Controller
{
    public function index()
    {
        $sql = "SELECT a.*, b.ten_danh_muc as ten_danh_muc_cha FROM `danh_mucs` a LEFT JOIN `danh_mucs` b ON a.id_danh_muc_cha = b.id ";
        $sql2 = "SELECT * FROM `danh_mucs` WHERE `id_danh_muc_cha` = 0";

        $data = DB::select($sql);
        $data2 = DB::select($sql2);

        return view('admin.pages.danh_muc.index', compact('data', 'data2'));
    }

    public function store(Request $request)
    {
        $slug_danh_muc = Str::Slug($request->ten_danh_muc);
        $now = Carbon::now('GMT+7');

        $sql = "SELECT * FROM `danh_mucs` WHERE `ma_Danh_muc` = '". $request->ma_danh_muc ."'";
        $data = DB::select($sql);

        if (count($data) > 0) {
            toastr()->error("Thêm mới thất bại!!!");
        } else {
            $sql ="INSERT INTO `danh_mucs`
            (`id`, `ma_Danh_muc`, `ten_danh_muc`, `slug_Danh_muc`, `is_open`, `created_at`, `updated_at`, `id_danh_muc_cha`)
            VALUES
            (NULL,
            '".$request->ma_danh_muc."',
            '".$request->ten_danh_muc."',
            '".$slug_danh_muc."',
            '".$request->is_open."',
            '".$now."',
            '".$now."',
            '".$request->id_danh_muc_cha."')";
            DB::insert($sql);
            toastr()->success("Thêm mới thành công!!!");
        }
        return redirect('/admin/danh-muc/index');
    }

    public function edit($id)
    {
        $sql = "SELECT * FROM `danh_mucs` WHERE `id` = '". $id ."'";
        $data = DB::select($sql);

        $sql2 = "SELECT * FROM `danh_mucs` WHERE `id_danh_muc_cha` = 0";
        $data2 = DB::select($sql2);

        if (count($data) > 0) {
            $danh_muc = $data[0];
            return view('admin.pages.danh_muc.edit', compact('danh_muc', 'id', 'data2'));
        } else {
            return redirect('/admin/danh-muc/index');
        }
    }

    public function update(Request $request)
    {
        $sql_select =  "SELECT *
                        FROM `danh_mucs`
                        WHERE `id` = $request->id";
        $data = DB::select($sql_select);
        if (count($data)> 0) {
            $sql_update =  "UPDATE `danh_mucs` SET
            `ma_Danh_muc` = '". $request->ma_danh_muc ."',
            `ten_danh_muc` = '". $request->ten_danh_muc ."',
            `is_open` = '". $request->is_open ."',
            `id_danh_muc_cha` = '". $request->id_danh_muc_cha ."',
            `updated_at` = '". Carbon::now() ."'
            WHERE `id` = $request->id";
            DB::update(($sql_update));
            toastr()->success("Cập nhập thành công!!!");
        } else {
            toastr()->error("Cập nhập thất bại!!!");
        }
        return redirect("/admin/danh-muc/index");
    }

    public function delete($id)
    {
        $sql_select =  "SELECT *
                        FROM `danh_mucs`
                        WHERE `id` = $id";

        $data = DB::select($sql_select);
        if (count($data)> 0) {
            $sql_delete = "delete from danh_mucs where id = '".$id."'";
            DB::delete($sql_delete);
            toastr()->success("Đã xoá thành công!!!");
        } else {
            toastr()->error("Danh mục không tồn tại!!!");
        }
        return redirect("/admin/danh-muc/index");
    }
}
