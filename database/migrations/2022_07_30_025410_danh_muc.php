<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('danh_mucs', function (Blueprint $table) {
            $table->id();
            $table->string('ma_Danh_muc')->unique();
            $table->string('ten_danh_muc')->unique();
            $table->string('slug_Danh_muc')->unique();
            $table->integer('is_open')->default(1);
            $table->integer('id_danh_muc_cha')->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
    }
};
