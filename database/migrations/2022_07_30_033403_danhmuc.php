<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('danh_mucs', function (Blueprint $table) {
            $table->integer('id_danh_muc_cha')->default(0);
        });
    }
    public function down()
    {

    }
};
