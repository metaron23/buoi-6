<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('san_phams', function (Blueprint $table) {
            $table->id();
            $table->string('ma_san_pham');
            $table->string('ten_san_pham');
            $table->string('slug_san_pham');
            $table->double('gia_ban', 18, 0);
            $table->double('gia_khuyen_mai', 18, 0)->nullable();
            $table->longText('ma_ta_ngan');
            $table->longText('ma_ta_chi_tiet');
            $table->string('hinh_anh')->default('no-image.png');
            $table->integer('is_open')->default(1);
            $table->integer('is_sell')->default(1);
            $table->integer('id_danh_muc')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
