@extends('admin.share.master')

@section('title')
    <h2>CẬP NHẬP DANH MỤC SẢN PHẨM</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="card" style="height: 963.469px;">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-tooltip">CẬP NHẬP DANH MỤC</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-minus"></i></a></li>
                            <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="feather icon-maximize"></i></a></li>
                            <li><a data-action="close"><i class="feather icon-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">

                        <div class="card-text">
                            <p>Đẩy là form để cập nhập <code>danh mục</code>
                            </p>
                        </div>

                        <form class="form" action="/admin/danh-muc/update" method="post">
                            @csrf
                            <div class="form-body">
                                <input type="text" value="{{ $danh_muc->id }}" hidden name="id">
                                <div class="form-group">
                                    <label>Mã Danh Mục</label>
                                    <input type="text" class="form-control"
                                        placeholder="Nhập vào mã danh mục" name="ma_danh_muc"
                                        value="{{ $danh_muc->ma_Danh_muc }}">
                                </div>
                                <div class="form-group">
                                    <label>Tên Danh Mục</label>
                                    <input type="text" class="form-control"
                                        placeholder="Nhập vào tên danh mục" name="ten_danh_muc"
                                        value="{{ $danh_muc->ten_danh_muc }}">
                                </div>

                                <div class="form-group">
                                    <label>Chọn tình trạng</label>
                                    <select name="is_open" class="form-control">
                                        <option value="1"
                                            {{ $danh_muc->is_open == 1 ? 'selected' : '' }}>Hiển thị
                                        </option>
                                        <option value="0"
                                            {{ $danh_muc->is_open == 0 ? 'selected' : '' }}>Tạm ẩn
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Danh Mục Cha</label>
                                    <select name="id_danh_muc_cha" class="form-control">
                                        <option value=0
                                            {{ $danh_muc->id_danh_muc_cha == 0 ? 'selected' : '' }}>
                                            Root</option>
                                        @foreach ($data2 as $key => $value)
                                            <option value={{ $value->id }}
                                                {{ $danh_muc->id_danh_muc_cha == $value->id ? 'selected' : '' }}>
                                                {{ $value->ten_danh_muc }}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>

                            </div>

                            <div class="form-actions">
                                <button type="reset" class="btn btn-warning mr-1">
                                    <i class="feather icon-x"></i> Huỷ
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-check-square-o"></i> Cập Nhập
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
