<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    @include('admin.share.head')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click"
    data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    @include('admin.share.top')
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    @include('admin.share.menu')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                @yield('title')
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('admin.share.foot')
    <!-- END: Footer-->


    @include('admin.share.bottom')

</body>
<!-- END: Body-->

</html>
