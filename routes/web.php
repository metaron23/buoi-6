<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\TestController::class, 'index']);

Route::get('/admin/danh-muc/index', [\App\Http\Controllers\DanhMucController::class, 'index']);
Route::post('/admin/danh-muc/index',[\App\Http\Controllers\DanhMucController::class, 'store']);

Route::get('/admin/danh-muc/edit/{id}', [\App\Http\Controllers\DanhMucController::class, 'edit']);
Route::post('/admin/danh-muc/update',[\App\Http\Controllers\DanhMucController::class, 'update']);
Route::get('/admin/danh-muc/delete/{id}', [\App\Http\Controllers\DanhMucController::class, 'delete']);


